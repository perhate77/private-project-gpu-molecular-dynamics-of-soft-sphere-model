**This is a parallel GPU Molecular dynamics code of Soft sphere potential model with Langevin thermostat, written in CUDA.**
   
 * I have added a comment with capital letters "TWEAK THIS" ahead of those variables which you can tweak and see the results
 	 
 * You must have the nvcc compiler installed.

 * To install nvcc compiler in linux, fist install recommended nvidia driver
    and then install cuda with the folloing command
    

 *      sudo apt install nvidia-cuda-toolkit

 *	 Compile code using: 
 *	 	 nvcc md_gpu_SoftSphereLD.cu -arch=sm_61 -lm -Xptxas -O3,-v --use_fast_math -w

      
      -arch=sm_61 is the flag used for the GPU card architecture. 
      -arch=sm_61 is used for Geforce GTX 1050 Ti (my machine’s GPU). To run above codes on Tesla V100, change -arch=sm_61 to -arch=sm_70.



 *	 Run :
 *		 nohup ./a.out &
 			
 

  Read input Data:<br/>
  <pre>
  Coordinate file   :: 		positionLAMMPS.dat (N=4000, number density = 1.2)
  Format:
    Particle Id		type	x	y	z	mx	my	mz 
    ....

  Velocity file   :: 		velocityLAMMPS.dat
  Format:
     Particle Id		vx	vy	vz
     ....</pre>

**Benchmarking with LAMMPS (Average potential energy)**


| T | LAMMPS Results, (\<PE\>) | CUDA Code Results, (\<PE\>)  |C Code Results, (\<PE\>) |
| ------ | ------ | ------ | ------ |
| 0.6 | 0.24088 |  0.24089 | 0.24092 |
| 1.0 | 0.26532 | 0.26521  | 0.26529 |

Average potential energy obtained from LAMMPS and
our CUDA code for soft sphere potential model at two temperature, T= 0.6, 1.0 and density,
ρ = 1.2. We have equilibrated the system for 10 6 MD steps and 1000 samples after every 10 4
MD steps are generated for averaging.
     
 Written By::<br/>
       **Rajneesh Kumar**
 *	Theoretical Sciences Unit, Jawaharlal Nehru Centre for Advanced Scientific Research,
 *	Bengaluru 560064, India.
 *	Email: rajneesh[at]jncasr.ac.in
 *	27 Dec, 2020
   

  

